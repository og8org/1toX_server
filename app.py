from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__)
import json
from elasticsearch import Elasticsearch
import datetime
import random
from flask_cors import CORS

CORS(app)

@app.route("/1tox/")
@app.route("/1toX/")
def index():
    es = Elasticsearch()
    try:
        hits = es.search('1tox',body=json.dumps({"query":{"match_all":{}},"sort":{"score":"desc"}}),_source=['name','score'],size=10)['hits']['hits']
        hsll = [ (x['_source']['name'],x['_source']['score']) for x in hits ]
    except Exception as e:
        hsll = []
    return render_template('index.html',hsll=hsll[:10])

@app.route("/1toX/addme/", methods=['GET'])
def addme():
    es = Elasticsearch()
    name = request.args.get('name',None)
    score = request.args.get('score',0)
    if len(name) < 1:
        return json.dumps({'err': "No Name"})
    body = json.dumps({'name':name,'score':score,'created': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M')})
    es.index('1tox',body=body)
    return json.dumps({'ACK': name})

@app.route("/1toX/geths/", methods=['GET'])
def geths():
    es = Elasticsearch()
    try:
        hits = es.search('1tox',body=json.dumps({"query":{"match_all":{}},"sort":{"score":"desc"}}),_source=['name','score'],size=10)['hits']['hits']
        hsll = [ (x['_source']['name'],x['_source']['score']) for x in hits ]
    except Exception as e:
        es.indices.create('1tox',body={"mappings" : { "properties" : { "score" : { "type" : "integer" } } }}) 
        for i in range(10):
            es.index("1tox",body={"name":"Allie", "score":0, 'created': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M')})
        hsll = [ ("Allie",0) ] *10
    return json.dumps({'hsll': hsll[:10]})

if __name__ == "__main__":
    app.run()
